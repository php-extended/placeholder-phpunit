# php-extended/placeholder-phpunit

A library that shows only the interfaces of phpunit, without requiring all the dependancies, for IDE autocompletion purposes.
Tests still requires phpunit libraries or phars to work !

![coverage](https://gitlab.com/php-extended/placeholder-phpunit/badges/master/pipeline.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar install php-extended/placeholder-phpunit ^9`


This library ^8 mocks phpunit 8.

This library ^9 mocks phpunit 9.


## License

MIT (See [license file](LICENSE)).

