<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/placeholder-phpunit library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PHPUnit\Framework\MockObject\Builder;

use Throwable;

/**
 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
 */
class InvocationMocker
{
	
	/**
	 * Adds a new method name match and returns the parameter match object for
	 * further matching possibilities.
	 *
	 * @param string $name Constraint for matching method, if a string is passed it will use the PHPUnit_Framework_Constraint_IsEqual
	 *
	 * @return InvocationMocker
	 */
	public function method($name)
	{
		return $this;
	}
	
	/**
	 * @param mixed $value
	 * @param mixed $nextValues
	 * @return InvocationMocker
	 */
	public function willReturn($value, ...$nextValues)// : InvocationMocker
	{
	return $this;
	}
	
	/**
	 * @param mixed $reference
	 *
	 * @return InvocationMocker
	 */
	public function willReturnReference(&$reference)// : InvocationMocker
	{
		return $this;
	}
	
	/**
	 * @param array<integer, array<integer, mixed>> $valueMap
	 *
	 * @return InvocationMocker
	 */
	public function willReturnMap(array $valueMap)// : InvocationMocker
	{
		return $this;
	}
	
	/**
	 * @param integer $argumentIndex
	 *
	 * @return InvocationMocker
	 */
	public function willReturnArgument($argumentIndex)// : InvocationMocker
	{
		return $this;
	}
	
	/**
	 * @param callable $callback
	 *
	 * @return InvocationMocker
	 */
	public function willReturnCallback($callback)// : InvocationMocker
	{
		return $this;
	}
	
	/**
	 * @return InvocationMocker
	 */
	public function willReturnSelf()// : InvocationMocker
	{
		return $this;
	}
	
	/**
	 * @param mixed $values
	 *
	 * @return InvocationMocker
	 */
	public function willReturnOnConsecutiveCalls(...$values)// : InvocationMocker
	{
		return $this;
	}
	
	/**
	 * @param Throwable $exception
	 * 
	 * @return InvocationMocker
	 */
	public function willThrowException(Throwable $exception)// : InvocationMocker
	{
		return $this;
	}
	
}
