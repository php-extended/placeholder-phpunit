<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/placeholder-phpunit library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PHPUnit\Framework\MockObject;

use Exception;
use RuntimeException;

/**
 * @template MockedType
 * @SuppressWarnings(PHPMD.UnusedFormalParameters)
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class MockBuilder
{
	
	/**
	 * Creates a mock object using a fluent interface.
	 *
	 * @throws RuntimeException
	 *
	 * @psalm-return MockObject&MockedType
	 * @psalm-suppress InvalidReturnType,InvalidReturnStatement
	 */
	public function getMock() : MockObject
	{
		/** @phpstan-ignore-next-line */
		return new MockObject();
	}
	
	/**
	 * Creates a mock object for an abstract class using a fluent interface.
	 *
	 * @throws Exception
	 *
	 * @psalm-return MockObject&MockedType
	 * @psalm-suppress InvalidReturnType,InvalidReturnStatement
	 */
	public function getMockForAbstractClass() : MockObject
	{
		/** @phpstan-ignore-next-line */
		return new MockObject();
	}
	
	/**
	 * Creates a mock object for a trait using a fluent interface.
	 *
	 * @throws Exception
	 *
	 * @psalm-return MockObject&MockedType
	 * @psalm-suppress InvalidReturnType,InvalidReturnStatement
	 */
	public function getMockForTrait() : MockObject
	{
		/** @phpstan-ignore-next-line */
		return new MockObject();
	}
	
	/**
	 * Specifies the subset of methods to mock, requiring each to exist in the class.
	 *
	 * @param array<integer|string, string> $methods
	 *
	 * @return $this
	 * @throws RuntimeException
	 */
	public function onlyMethods(array $methods) : MockBuilder
	{
		return $this;
	}
	
	/**
	 * Specifies methods that don't exist in the class which you want to mock.
	 *
	 * @param array<integer|string, string> $methods
	 *
	 * @return $this
	 * @throws RuntimeException
	 */
	public function addMethods(array $methods) : MockBuilder
	{
		return $this;
	}
	
	/**
	 * Specifies the subset of methods to not mock. Default is to mock all of them.
	 * 
	 * @param array<integer|string, string> $methods
	 *
	 * @return $this
	 * @throws RuntimeException
	 */
	public function setMethodsExcept(array $methods = []) : MockBuilder
	{
		return $this;
	}
	
	/**
	 * Specifies the arguments for the constructor.
	 * 
	 * @param array<integer|string, string> $args
	 *
	 * @return $this
	 */
	public function setConstructorArgs(array $args) : MockBuilder
	{
		return $this;
	}
	
	/**
	 * Specifies the name for the mock class.
	 * 
	 * @param string $name
	 *
	 * @return $this
	 */
	public function setMockClassName(string $name) : MockBuilder
	{
		return $this;
	}
	
	/**
	 * Disables the invocation of the original constructor.
	 *
	 * @return $this
	 */
	public function disableOriginalConstructor() : MockBuilder
	{
		return $this;
	}
	
	/**
	 * Enables the invocation of the original constructor.
	 *
	 * @return $this
	 */
	public function enableOriginalConstructor() : MockBuilder
	{
		return $this;
	}
	
	/**
	 * Disables the invocation of the original clone constructor.
	 *
	 * @return $this
	 */
	public function disableOriginalClone() : MockBuilder
	{
		return $this;
	}
	
	/**
	 * Enables the invocation of the original clone constructor.
	 *
	 * @return $this
	 */
	public function enableOriginalClone() : MockBuilder
	{
		return $this;
	}
	
	/**
	 * Disables the use of class autoloading while creating the mock object.
	 *
	 * @return $this
	 */
	public function disableAutoload() : MockBuilder
	{
		return $this;
	}
	
	/**
	 * Enables the use of class autoloading while creating the mock object.
	 *
	 * @return $this
	 */
	public function enableAutoload() : MockBuilder
	{
		return $this;
	}
	
	/**
	 * Disables the cloning of arguments passed to mocked methods.
	 *
	 * @return $this
	 */
	public function disableArgumentCloning() : MockBuilder
	{
		return $this;
	}
	
	/**
	 * Enables the cloning of arguments passed to mocked methods.
	 *
	 * @return $this
	 */
	public function enableArgumentCloning() : MockBuilder
	{
		return $this;
	}
	
	/**
	 * Enables the invocation of the original methods.
	 *
	 * @return $this
	 */
	public function enableProxyingToOriginalMethods() : MockBuilder
	{
		return $this;
	}
	
	/**
	 * Disables the invocation of the original methods.
	 *
	 * @return $this
	 */
	public function disableProxyingToOriginalMethods() : MockBuilder
	{
		return $this;
	}
	
	/**
	 * Sets the proxy target.
	 *
	 * @param mixed $object
	 * @return $this
	 */
	public function setProxyTarget($object) : MockBuilder
	{
		return $this;
	}
	
	/**
	 * @return $this
	 */
	public function allowMockingUnknownTypes() : MockBuilder
	{
		return $this;
	}
	
	/**
	 * @return $this
	 */
	public function disallowMockingUnknownTypes() : MockBuilder
	{
		return $this;
	}
	
	/**
	 * @return $this
	 */
	public function enableAutoReturnValueGeneration() : MockBuilder
	{
		return $this;
	}
	
	/**
	 * @return $this
	 */
	public function disableAutoReturnValueGeneration() : MockBuilder
	{
		return $this;
	}
	
}
