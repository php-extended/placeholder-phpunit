<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/placeholder-phpunit library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PHPUnit\Framework\MockObject;

use PHPUnit\Framework\MockObject\Rule\InvocationOrder;

/**
 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
 */
class Stub
{
	
	public function invoke(InvocationOrder $invocation) : void {}
	
}
