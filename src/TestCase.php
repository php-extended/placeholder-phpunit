<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/placeholder-phpunit library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PHPUnit\Framework;

use ArrayAccess;
use Countable;
use DOMDocument;
use Exception;
use PHPUnit\Framework\MockObject\MockBuilder;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\MockObject\Rule\InvocationOrder;
use PHPUnit\Framework\MockObject\Stub;
use stdClass;
use Throwable;

/**
 * @SuppressWarnings(PHPMD.UnusedFormalParameters)
 * @SuppressWarnings(PHPMD.ExcessiveClassLength)
 * @SuppressWarnings(PHPMD.ExcessivePublicCount)
 * @SuppressWarnings(PHPMD.TooManyMethods)
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 * @SuppressWarnings(PHPMD.ShortMethodName)
 * @SuppressWarnings(PHPMD.ShortVariable)
 *
 * @internal
 *
 * @small
 * @coversNothing
 */
class TestCase
{
	
	/**
	 * Asserts that an array has a specified key.
	 *
	 * @param integer|string $key
	 * @param array<integer|string, mixed>|ArrayAccess<array-key, mixed> $array
	 * @param string $message
	 *
	 * @throws Exception
	 */
	public static function assertArrayHasKey($key, $array, string $message = '') : void {}
	
	/**
	 * Asserts that an array does not have a specified key.
	 *
	 * @param integer|string $key
	 * @param array<integer|string, mixed>|ArrayAccess<array-key, mixed> $array
	 * @param string $message
	 *
	 * @throws Exception
	 */
	public static function assertArrayNotHasKey($key, $array, string $message = '') : void {}
	
	/**
	 * Asserts that a haystack contains a needle.
	 *
	 * @param mixed $needle
	 * @param mixed $haystack
	 * @param string $message
	 * @param boolean $ignoreCase
	 * @param boolean $checkForObjectIdentity
	 * @param boolean $checkForNonObjectIdentity
	 * @throws Exception
	 */
	public static function assertContains($needle, $haystack, string $message = '', bool $ignoreCase = false, bool $checkForObjectIdentity = true, bool $checkForNonObjectIdentity = false) : void {}
	
	/**
	 * Asserts that a haystack contains a needle.
	 *
	 * @param mixed $needle
	 * @param iterable<mixed> $haystack
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertContainsEquals($needle, iterable $haystack, string $message = '') : void {}
	
	/**
	 * Asserts that a haystack does not contain a needle.
	 *
	 * @param mixed $needle
	 * @param mixed $haystack
	 * @param string $message
	 * @param boolean $ignoreCase
	 * @param boolean $checkForObjectIdentity
	 * @param boolean $checkForNonObjectIdentity
	 * @throws Exception
	 */
	public static function assertNotContains($needle, $haystack, string $message = '', bool $ignoreCase = false, bool $checkForObjectIdentity = true, bool $checkForNonObjectIdentity = false) : void {}
	
	/**
	 * Asserts that a haystack does not contain a needle.
	 *
	 * @param mixed $needle
	 * @param iterable<mixed> $haystack
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertNotContainsEquals($needle, iterable $haystack, string $message = '') : void {}
	
	/**
	 * Asserts that a haystack contains only values of a given type.
	 *
	 * @param string $type
	 * @param iterable<mixed> $haystack
	 * @param ?boolean $isNativeType
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertContainsOnly(string $type, iterable $haystack, ?bool $isNativeType = null, string $message = '') : void {}
	
	/**
	 * Asserts that a haystack contains only instances of a given class name.
	 *
	 * @param string $className
	 * @param iterable<mixed> $haystack
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertContainsOnlyInstancesOf(string $className, iterable $haystack, string $message = '') : void {}
	
	/**
	 * Asserts that a haystack does not contain only values of a given type.
	 *
	 * @param string $type
	 * @param iterable<mixed> $haystack
	 * @param ?boolean $isNativeType
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertNotContainsOnly(string $type, iterable $haystack, ?bool $isNativeType = null, string $message = '') : void {}
	
	/**
	 * Asserts the number of elements of an array, Countable or Traversable.
	 *
	 * @param integer $expectedCount
	 * @param Countable|iterable<mixed> $haystack
	 * @param string $message
	 *
	 * @throws Exception
	 */
	public static function assertCount(int $expectedCount, $haystack, string $message = '') : void {}
	
	/**
	 * Asserts the number of elements of an array, Countable or Traversable.
	 *
	 * @param integer $expectedCount
	 * @param Countable|iterable<mixed> $haystack
	 * @param string $message
	 *
	 * @throws Exception
	 */
	public static function assertNotCount(int $expectedCount, $haystack, string $message = '') : void {}
	
	/**
	 * Asserts that two variables are equal.
	 *
	 * @param mixed $expected
	 * @param mixed $actual
	 * @param string $message
	 * @param float $delta
	 * @param integer $maxDepth
	 * @param boolean $canonicalize
	 * @param boolean $ignoreCase
	 * @throws Exception
	 */
	public static function assertEquals($expected, $actual, string $message = '', float $delta = 0.0, int $maxDepth = 10, bool $canonicalize = false, bool $ignoreCase = false) : void {}
	
	/**
	 * Asserts that two variables are equal (canonicalizing).
	 *
	 * @param mixed $expected
	 * @param mixed $actual
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertEqualsCanonicalizing($expected, $actual, string $message = '') : void {}
	
	/**
	 * Asserts that two variables are equal (ignoring case).
	 *
	 * @param mixed $expected
	 * @param mixed $actual
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertEqualsIgnoringCase($expected, $actual, string $message = '') : void {}
	
	/**
	 * Asserts that two variables are equal (with delta).
	 *
	 * @param mixed $expected
	 * @param mixed $actual
	 * @param float $delta
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertEqualsWithDelta($expected, $actual, float $delta, string $message = '') : void {}
	
	/**
	 * Asserts that two variables are not equal.
	 *
	 * @param mixed $expected
	 * @param mixed $actual
	 * @param string $message
	 * @param float $delta
	 * @param integer $maxDepth
	 * @param boolean $canonicalize
	 * @param boolean $ignoreCase
	 * @throws Exception
	 */
	public static function assertNotEquals($expected, $actual, string $message = '', $delta = 0.0, $maxDepth = 10, $canonicalize = false, $ignoreCase = false) : void {}
	
	/**
	 * Asserts that two variables are not equal (canonicalizing).
	 *
	 * @param mixed $expected
	 * @param mixed $actual
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertNotEqualsCanonicalizing($expected, $actual, string $message = '') : void {}
	
	/**
	 * Asserts that two variables are not equal (ignoring case).
	 *
	 * @param mixed $expected
	 * @param mixed $actual
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertNotEqualsIgnoringCase($expected, $actual, string $message = '') : void {}
	
	/**
	 * Asserts that two variables are not equal (with delta).
	 *
	 * @param mixed $expected
	 * @param mixed $actual
	 * @param float $delta
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertNotEqualsWithDelta($expected, $actual, float $delta, string $message = '') : void {}
	
	/**
	 * Assets that two objects are equal.
	 * 
	 * @param object $expected
	 * @param object $actual
	 * @param string $method
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertObjectEquals(object $expected, object $actual, string $method = 'equals', string $message = '') : void {}
	
	/**
	 * Asserts that a variable is empty.
	 *
	 * @psalm-assert empty $actual
	 * @param mixed $actual
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertEmpty($actual, string $message = '') : void {}
	
	/**
	 * Asserts that a variable is not empty.
	 *
	 * @psalm-assert !empty $actual
	 * @param mixed $actual
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertNotEmpty($actual, string $message = '') : void {}
	
	/**
	 * Asserts that a value is greater than another value.
	 *
	 * @param mixed $expected
	 * @param mixed $actual
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertGreaterThan($expected, $actual, string $message = '') : void {}
	
	/**
	 * Asserts that a value is greater than or equal to another value.
	 *
	 * @param mixed $expected
	 * @param mixed $actual
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertGreaterThanOrEqual($expected, $actual, string $message = '') : void {}
	
	/**
	 * Asserts that a value is smaller than another value.
	 *
	 * @param mixed $expected
	 * @param mixed $actual
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertLessThan($expected, $actual, string $message = '') : void {}
	
	/**
	 * Asserts that a value is smaller than or equal to another value.
	 *
	 * @param mixed $expected
	 * @param mixed $actual
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertLessThanOrEqual($expected, $actual, string $message = '') : void {}
	
	/**
	 * Asserts that the contents of one file is equal to the contents of another
	 * file.
	 *
	 * @param string $expected
	 * @param string $actual
	 * @param string $message
	 * @param boolean $canonicalize
	 * @param boolean $ignoreCase
	 * @throws Exception
	 */
	public static function assertFileEquals(string $expected, string $actual, string $message = '', bool $canonicalize = false, bool $ignoreCase = false) : void {}
	
	/**
	 * Asserts that the contents of one file is equal to the contents of another
	 * file (canonicalizing).
	 *
	 * @param string $expected
	 * @param string $actual
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertFileEqualsCanonicalizing(string $expected, string $actual, string $message = '') : void {}
	
	/**
	 * Asserts that the contents of one file is equal to the contents of another
	 * file (ignoring case).
	 *
	 * @param string $expected
	 * @param string $actual
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertFileEqualsIgnoringCase(string $expected, string $actual, string $message = '') : void {}
	
	/**
	 * Asserts that the contents of one file is not equal to the contents of
	 * another file.
	 *
	 * @param string $expected
	 * @param string $actual
	 * @param string $message
	 * @param boolean $canonicalize
	 * @param boolean $ignoreCase
	 * @throws Exception
	 */
	public static function assertFileNotEquals(string $expected, string $actual, string $message = '', bool $canonicalize = false, bool $ignoreCase = false) : void {}
	
	/**
	 * Asserts that the contents of one file is not equal to the contents of another
	 * file (canonicalizing).
	 *
	 * @param string $expected
	 * @param string $actual
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertFileNotEqualsCanonicalizing(string $expected, string $actual, string $message = '') : void {}
	
	/**
	 * Asserts that the contents of one file is not equal to the contents of another
	 * file (ignoring case).
	 *
	 * @param string $expected
	 * @param string $actual
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertFileNotEqualsIgnoringCase(string $expected, string $actual, string $message = '') : void {}
	
	/**
	 * Asserts that the contents of a string is equal
	 * to the contents of a file.
	 *
	 * @param string $expectedFile
	 * @param string $actualString
	 * @param string $message
	 * @param boolean $canonicalize
	 * @param boolean $ignoreCase
	 * @throws Exception
	 */
	public static function assertStringEqualsFile(string $expectedFile, string $actualString, string $message = '', bool $canonicalize = false, bool $ignoreCase = false) : void {}
	
	/**
	 * Asserts that the contents of a string is equal
	 * to the contents of a file (canonicalizing).
	 *
	 * @param string $expectedFile
	 * @param string $actualString
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertStringEqualsFileCanonicalizing(string $expectedFile, string $actualString, string $message = '') : void {}
	
	/**
	 * Asserts that the contents of a string is equal
	 * to the contents of a file (ignoring case).
	 *
	 * @param string $expectedFile
	 * @param string $actualString
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertStringEqualsFileIgnoringCase(string $expectedFile, string $actualString, string $message = '') : void {}
	
	/**
	 * Asserts that the contents of a string is not equal
	 * to the contents of a file.
	 *
	 * @param string $expectedFile
	 * @param string $actualString
	 * @param string $message
	 * @param boolean $canonicalize
	 * @param boolean $ignoreCase
	 * @throws Exception
	 */
	public static function assertStringNotEqualsFile(string $expectedFile, string $actualString, string $message = '', bool $canonicalize = false, bool $ignoreCase = false) : void {}
	
	/**
	 * Asserts that the contents of a string is not equal
	 * to the contents of a file (canonicalizing).
	 *
	 * @param string $expectedFile
	 * @param string $actualString
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertStringNotEqualsFileCanonicalizing(string $expectedFile, string $actualString, string $message = '') : void {}
	
	/**
	 * Asserts that the contents of a string is not equal
	 * to the contents of a file (ignoring case).
	 *
	 * @param string $expectedFile
	 * @param string $actualString
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertStringNotEqualsFileIgnoringCase(string $expectedFile, string $actualString, string $message = '') : void {}
	
	/**
	 * Asserts that a file/dir is readable.
	 *
	 * @param string $filename
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertIsReadable(string $filename, string $message = '') : void {}
	
	/**
	 * Asserts that a file/dir exists and is not readable.
	 *
	 * @param string $filename
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertIsNotReadable(string $filename, string $message = '') : void {}
	
	/**
	 * Asserts that a file/dir exists and is writable.
	 *
	 * @param string $filename
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertIsWritable(string $filename, string $message = '') : void {}
	
	/**
	 * Asserts that a file/dir exists and is not writable.
	 *
	 * @param string $filename
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertIsNotWritable(string $filename, string $message = '') : void {}
	
	/**
	 * Asserts that a directory exists.
	 *
	 * @param string $directory
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertDirectoryExists(string $directory, string $message = '') : void {}
	
	/**
	 * Asserts that a directory does not exist.
	 *
	 * @param string $directory
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertDirectoryDoesNotExist(string $directory, string $message = '') : void {}
	
	/**
	 * Asserts that a directory exists and is readable.
	 *
	 * @param string $directory
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertDirectoryIsReadable(string $directory, string $message = '') : void {}
	
	/**
	 * Asserts that a directory exists and is not readable.
	 *
	 * @param string $directory
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertDirectoryIsNotReadable(string $directory, string $message = '') : void {}
	
	/**
	 * Asserts that a directory exists and is writable.
	 *
	 * @param string $directory
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertDirectoryIsWritable(string $directory, string $message = '') : void {}
	
	/**
	 * Asserts that a directory exists and is not writable.
	 *
	 * @param string $directory
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertDirectoryIsNotWritable(string $directory, string $message = '') : void {}
	
	/**
	 * Asserts that a file exists.
	 *
	 * @param string $filename
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertFileExists(string $filename, string $message = '') : void {}
	
	/**
	 * Asserts that a file does not exist.
	 *
	 * @param string $filename
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertFileDoesNotExist(string $filename, string $message = '') : void {}
	
	/**
	 * Asserts that a file exists and is readable.
	 *
	 * @param string $file
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertFileIsReadable(string $file, string $message = '') : void {}
	
	/**
	 * Asserts that a file exists and is not readable.
	 *
	 * @param string $file
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertFileIsNotReadable(string $file, string $message = '') : void {}
	
	/**
	 * Asserts that a file exists and is writable.
	 *
	 * @param string $file
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertFileIsWritable(string $file, string $message = '') : void {}
	
	/**
	 * Asserts that a file exists and is not writable.
	 *
	 * @param string $file
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertFileIsNotWritable(string $file, string $message = '') : void {}
	
	/**
	 * Asserts that a condition is true.
	 *
	 * @psalm-assert true $condition
	 * @param mixed $condition
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertTrue($condition, string $message = '') : void {}
	
	/**
	 * Asserts that a condition is not true.
	 *
	 * @psalm-assert !true $condition
	 * @param mixed $condition
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertNotTrue($condition, string $message = '') : void {}
	
	/**
	 * Asserts that a condition is false.
	 *
	 * @psalm-assert false $condition
	 * @param mixed $condition
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertFalse($condition, string $message = '') : void {}
	
	/**
	 * Asserts that a condition is not false.
	 *
	 * @psalm-assert !false $condition
	 * @param mixed $condition
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertNotFalse($condition, string $message = '') : void {}
	
	/**
	 * Asserts that a variable is null.
	 *
	 * @psalm-assert null $actual
	 * @param mixed $actual
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertNull($actual, string $message = '') : void {}
	
	/**
	 * Asserts that a variable is not null.
	 *
	 * @psalm-assert !null $actual
	 * @param mixed $actual
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertNotNull($actual, string $message = '') : void {}
	
	/**
	 * Asserts that a variable is finite.
	 *
	 * @param mixed $actual
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertFinite($actual, string $message = '') : void {}
	
	/**
	 * Asserts that a variable is infinite.
	 *
	 * @param mixed $actual
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertInfinite($actual, string $message = '') : void {}
	
	/**
	 * Asserts that a variable is nan.
	 *
	 * @param mixed $actual
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertNan($actual, string $message = '') : void {}
	
	/**
	 * Asserts that a class has a specified attribute.
	 *
	 * @param string $attributeName
	 * @param string $className
	 * @param string $message
	 * @throws Exception
	 * @throws Exception
	 */
	public static function assertClassHasAttribute(string $attributeName, string $className, string $message = '') : void {}
	
	/**
	 * Asserts that a class does not have a specified attribute.
	 *
	 * @param string $attributeName
	 * @param string $className
	 * @param string $message
	 * @throws Exception
	 * @throws Exception
	 */
	public static function assertClassNotHasAttribute(string $attributeName, string $className, string $message = '') : void {}
	
	/**
	 * Asserts that a class has a specified static attribute.
	 *
	 * @param string $attributeName
	 * @param string $className
	 * @param string $message
	 * @throws Exception
	 * @throws Exception
	 */
	public static function assertClassHasStaticAttribute(string $attributeName, string $className, string $message = '') : void {}
	
	/**
	 * Asserts that a class does not have a specified static attribute.
	 *
	 * @param string $attributeName
	 * @param string $className
	 * @param string $message
	 * @throws Exception
	 * @throws Exception
	 */
	public static function assertClassNotHasStaticAttribute(string $attributeName, string $className, string $message = '') : void {}
	
	/**
	 * Asserts that an object has a specified attribute.
	 *
	 * @param string $attributeName
	 * @param object $object
	 * @param string $message
	 *
	 * @throws Exception
	 * @throws Exception
	 */
	public static function assertObjectHasAttribute(string $attributeName, $object, string $message = '') : void {}
	
	/**
	 * Asserts that an object does not have a specified attribute.
	 *
	 * @param string $attributeName
	 * @param object $object
	 * @param string $message
	 *
	 * @throws Exception
	 * @throws Exception
	 */
	public static function assertObjectNotHasAttribute(string $attributeName, $object, string $message = '') : void {}
	
	/**
	 * Asserts that two variables have the same type and value.
	 * Used on objects, it asserts that two variables reference
	 * the same object.
	 *
	 * @phpstan-template ExpectedType
	 * @phpstan-param ExpectedType $expected
	 * @psalm-assert =ExpectedType $actual
	 * @param mixed $expected
	 * @param mixed $actual
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertSame($expected, $actual, string $message = '') : void {}
	
	/**
	 * Asserts that two variables do not have the same type and value.
	 * Used on objects, it asserts that two variables do not reference
	 * the same object.
	 *
	 * @param mixed $expected
	 * @param mixed $actual
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertNotSame($expected, $actual, string $message = '') : void {}
	
	/**
	 * Asserts that a variable is of a given type.
	 *
	 * @phpstan-template ExpectedType of object
	 * @phpstan-param class-string<ExpectedType> $expected
	 * @psalm-assert =ExpectedType $actual
	 * @param string $expected
	 * @param mixed $actual
	 * @param string $message
	 * @throws Exception
	 * @throws Exception
	 */
	public static function assertInstanceOf(string $expected, $actual, string $message = '') : void {}
	
	/**
	 * Asserts that a variable is not of a given type.
	 *
	 * @phpstan-template ExpectedType of object
	 * @phpstan-param class-string<ExpectedType> $expected
	 * @psalm-assert !ExpectedType $actual
	 * @param string $expected
	 * @param mixed $actual
	 * @param string $message
	 * @throws Exception
	 * @throws Exception
	 */
	public static function assertNotInstanceOf(string $expected, $actual, string $message = '') : void {}
	
	/**
	 * Asserts that a variable is of type array.
	 *
	 * @psalm-assert array $actual
	 * @param mixed $actual
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertIsArray($actual, string $message = '') : void {}
	
	/**
	 * Asserts that a variable is of type bool.
	 *
	 * @psalm-assert bool $actual
	 * @param mixed $actual
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertIsBool($actual, string $message = '') : void {}
	
	/**
	 * Asserts that a variable is of type float.
	 *
	 * @psalm-assert float $actual
	 * @param mixed $actual
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertIsFloat($actual, string $message = '') : void {}
	
	/**
	 * Asserts that a variable is of type int.
	 *
	 * @psalm-assert int $actual
	 * @param mixed $actual
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertIsInt($actual, string $message = '') : void {}
	
	/**
	 * Asserts that a variable is of type numeric.
	 *
	 * @psalm-assert numeric $actual
	 * @param mixed $actual
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertIsNumeric($actual, string $message = '') : void {}
	
	/**
	 * Asserts that a variable is of type object.
	 *
	 * @psalm-assert object $actual
	 * @param mixed $actual
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertIsObject($actual, string $message = '') : void {}
	
	/**
	 * Asserts that a variable is of type resource.
	 *
	 * @psalm-assert resource $actual
	 * @param mixed $actual
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertIsResource($actual, string $message = '') : void {}
	
	/**
	 * Asserts that a variable is of type resource and is closed.
	 * 
	 * @psalm-assert resource $actual
	 * @param mixed $actual
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertIsClosedResource($actual, string $message = '') : void {}
	
	/**
	 * Asserts that a variable is of type string.
	 *
	 * @psalm-assert string $actual
	 * @param mixed $actual
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertIsString($actual, string $message = '') : void {}
	
	/**
	 * Asserts that a variable is of type scalar.
	 *
	 * @psalm-assert scalar $actual
	 * @param mixed $actual
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertIsScalar($actual, string $message = '') : void {}
	
	/**
	 * Asserts that a variable is of type callable.
	 *
	 * @psalm-assert callable $actual
	 * @param mixed $actual
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertIsCallable($actual, string $message = '') : void {}
	
	/**
	 * Asserts that a variable is of type iterable.
	 *
	 * @psalm-assert iterable<mixed> $actual
	 * @param mixed $actual
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertIsIterable($actual, string $message = '') : void {}
	
	/**
	 * Asserts that a variable is not of type array.
	 *
	 * @psalm-assert !array<mixed> $actual
	 * @param mixed $actual
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertIsNotArray($actual, string $message = '') : void {}
	
	/**
	 * Asserts that a variable is not of type bool.
	 *
	 * @psalm-assert !bool $actual
	 * @param mixed $actual
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertIsNotBool($actual, string $message = '') : void {}
	
	/**
	 * Asserts that a variable is not of type float.
	 *
	 * @psalm-assert !float $actual
	 * @param mixed $actual
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertIsNotFloat($actual, string $message = '') : void {}
	
	/**
	 * Asserts that a variable is not of type int.
	 *
	 * @psalm-assert !int $actual
	 * @param mixed $actual
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertIsNotInt($actual, string $message = '') : void {}
	
	/**
	 * Asserts that a variable is not of type numeric.
	 *
	 * @psalm-assert !numeric $actual
	 * @param mixed $actual
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertIsNotNumeric($actual, string $message = '') : void {}
	
	/**
	 * Asserts that a variable is not of type object.
	 *
	 * @psalm-assert !object $actual
	 * @param mixed $actual
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertIsNotObject($actual, string $message = '') : void {}
	
	/**
	 * Asserts that a variable is not of type resource.
	 *
	 * @psalm-assert !resource $actual
	 * @param mixed $actual
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertIsNotResource($actual, string $message = '') : void {}
	
	/**
	 * Asserts that a variable is not of type resource.
	 *
	 * @psalm-assert !resource $actual
	 * @param mixed $actual
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertIsNotClosedResource($actual, string $message = '') : void {}
	
	/**
	 * Asserts that a variable is not of type string.
	 *
	 * @psalm-assert !string $actual
	 * @param mixed $actual
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertIsNotString($actual, string $message = '') : void {}
	
	/**
	 * Asserts that a variable is not of type scalar.
	 *
	 * @psalm-assert !scalar $actual
	 * @param mixed $actual
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertIsNotScalar($actual, string $message = '') : void {}
	
	/**
	 * Asserts that a variable is not of type callable.
	 *
	 * @psalm-assert !callable $actual
	 * @param mixed $actual
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertIsNotCallable($actual, string $message = '') : void {}
	
	/**
	 * Asserts that a variable is not of type iterable.
	 *
	 * @psalm-assert !iterable $actual
	 * @param mixed $actual
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertIsNotIterable($actual, string $message = '') : void {}
	
	/**
	 * Asserts that a string matches a given regular expression.
	 *
	 * @param string $pattern
	 * @param string $string
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertMatchesRegularExpression(string $pattern, string $string, string $message = '') : void {}
	
	/**
	 * Asserts that a string does not match a given regular expression.
	 *
	 * @param string $pattern
	 * @param string $string
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertDoesNotMatchRegularExpression(string $pattern, string $string, string $message = '') : void {}
	
	/**
	 * Assert that the size of two arrays (or `Countable` or `Traversable` objects)
	 * is the same.
	 *
	 * @param Countable|iterable<mixed> $expected
	 * @param Countable|iterable<mixed> $actual
	 * @param string $message
	 *
	 * @throws Exception
	 * @throws Exception
	 */
	public static function assertSameSize($expected, $actual, string $message = '') : void {}
	
	/**
	 * Assert that the size of two arrays (or `Countable` or `Traversable` objects)
	 * is not the same.
	 *
	 * @param Countable|iterable<mixed> $expected
	 * @param Countable|iterable<mixed> $actual
	 * @param string $message
	 *
	 * @throws Exception
	 * @throws Exception
	 */
	public static function assertNotSameSize($expected, $actual, string $message = '') : void {}
	
	/**
	 * Asserts that a string matches a given format string.
	 *
	 * @param string $format
	 * @param string $string
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertStringMatchesFormat(string $format, string $string, string $message = '') : void {}
	
	/**
	 * Asserts that a string does not match a given format string.
	 *
	 * @param string $format
	 * @param string $string
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertStringNotMatchesFormat(string $format, string $string, string $message = '') : void {}
	
	/**
	 * Asserts that a string matches a given format file.
	 *
	 * @param string $formatFile
	 * @param string $string
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertStringMatchesFormatFile(string $formatFile, string $string, string $message = '') : void {}
	
	/**
	 * Asserts that a string does not match a given format string.
	 *
	 * @param string $formatFile
	 * @param string $string
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertStringNotMatchesFormatFile(string $formatFile, string $string, string $message = '') : void {}
	
	/**
	 * Asserts that a string starts with a given prefix.
	 *
	 * @param string $prefix
	 * @param string $string
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertStringStartsWith(string $prefix, string $string, string $message = '') : void {}
	
	/**
	 * Asserts that a string starts not with a given prefix.
	 *
	 * @param string $prefix
	 * @param string $string
	 * @param string $message
	 *
	 * @throws Exception
	 */
	public static function assertStringStartsNotWith($prefix, $string, string $message = '') : void {}
	
	/**
	 * @param string $needle
	 * @param string $haystack
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertStringContainsString(string $needle, string $haystack, string $message = '') : void {}
	
	/**
	 * @param string $needle
	 * @param string $haystack
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertStringContainsStringIgnoringCase(string $needle, string $haystack, string $message = '') : void {}
	
	/**
	 * @param string $needle
	 * @param string $haystack
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertStringNotContainsString(string $needle, string $haystack, string $message = '') : void {}
	
	/**
	 * @param string $needle
	 * @param string $haystack
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertStringNotContainsStringIgnoringCase(string $needle, string $haystack, string $message = '') : void {}
	
	/**
	 * Asserts that a string ends with a given suffix.
	 *
	 * @param string $suffix
	 * @param string $string
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertStringEndsWith(string $suffix, string $string, string $message = '') : void {}
	
	/**
	 * Asserts that a string ends not with a given suffix.
	 *
	 * @param string $suffix
	 * @param string $string
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertStringEndsNotWith(string $suffix, string $string, string $message = '') : void {}
	
	/**
	 * Asserts that two XML files are equal.
	 *
	 * @param string $expectedFile
	 * @param string $actualFile
	 * @param string $message
	 * @throws Exception
	 * @throws Exception
	 */
	public static function assertXmlFileEqualsXmlFile(string $expectedFile, string $actualFile, string $message = '') : void {}
	
	/**
	 * Asserts that two XML files are not equal.
	 *
	 * @param string $expectedFile
	 * @param string $actualFile
	 * @param string $message
	 * @throws Exception
	 * @throws Exception
	 */
	public static function assertXmlFileNotEqualsXmlFile(string $expectedFile, string $actualFile, string $message = '') : void {}
	
	/**
	 * Asserts that two XML documents are equal.
	 *
	 * @param string $expectedFile
	 * @param DOMDocument|string $actualXml
	 * @param string $message
	 *
	 * @throws Exception
	 * @throws Exception
	 */
	public static function assertXmlStringEqualsXmlFile(string $expectedFile, $actualXml, string $message = '') : void {}
	
	/**
	 * Asserts that two XML documents are not equal.
	 *
	 * @param string $expectedFile
	 * @param DOMDocument|string $actualXml
	 * @param string $message
	 *
	 * @throws Exception
	 * @throws Exception
	 */
	public static function assertXmlStringNotEqualsXmlFile(string $expectedFile, $actualXml, string $message = '') : void {}
	
	/**
	 * Asserts that two XML documents are equal.
	 *
	 * @param DOMDocument|string $expectedXml
	 * @param DOMDocument|string $actualXml
	 * @param string $message
	 *
	 * @throws Exception
	 * @throws Exception
	 */
	public static function assertXmlStringEqualsXmlString($expectedXml, $actualXml, string $message = '') : void {}
	
	/**
	 * Asserts that two XML documents are not equal.
	 *
	 * @param DOMDocument|string $expectedXml
	 * @param DOMDocument|string $actualXml
	 * @param string $message
	 *
	 * @throws Exception
	 * @throws Exception
	 */
	public static function assertXmlStringNotEqualsXmlString($expectedXml, $actualXml, string $message = '') : void {}
	
	/**
	 * Asserts that a string is a valid JSON string.
	 *
	 * @param string $actualJson
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertJson(string $actualJson, string $message = '') : void {}
	
	/**
	 * Asserts that two given JSON encoded objects or arrays are equal.
	 *
	 * @param string $expectedJson
	 * @param string $actualJson
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertJsonStringEqualsJsonString(string $expectedJson, string $actualJson, string $message = '') : void {}
	
	/**
	 * Asserts that two given JSON encoded objects or arrays are not equal.
	 *
	 * @param string $expectedJson
	 * @param string $actualJson
	 * @param string $message
	 *
	 * @throws Exception
	 */
	public static function assertJsonStringNotEqualsJsonString($expectedJson, $actualJson, string $message = '') : void {}
	
	/**
	 * Asserts that the generated JSON encoded object and the content of the given file are equal.
	 *
	 * @param string $expectedFile
	 * @param string $actualJson
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertJsonStringEqualsJsonFile(string $expectedFile, string $actualJson, string $message = '') : void {}
	
	/**
	 * Asserts that the generated JSON encoded object and the content of the given file are not equal.
	 *
	 * @param string $expectedFile
	 * @param string $actualJson
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertJsonStringNotEqualsJsonFile(string $expectedFile, string $actualJson, string $message = '') : void {}
	
	/**
	 * Asserts that two JSON files are equal.
	 *
	 * @param string $expectedFile
	 * @param string $actualFile
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertJsonFileEqualsJsonFile(string $expectedFile, string $actualFile, string $message = '') : void {}
	
	/**
	 * Asserts that two JSON files are not equal.
	 *
	 * @param string $expectedFile
	 * @param string $actualFile
	 * @param string $message
	 * @throws Exception
	 */
	public static function assertJsonFileNotEqualsJsonFile(string $expectedFile, string $actualFile, string $message = '') : void {}
	
	/**
	 * Fails a test with the given message.
	 *
	 * @param string $message
	 * @throws Exception
	 */
	public static function fail(string $message = '') : void {}
	
	/**
	 * Mark the test as incomplete.
	 *
	 * @param string $message
	 * @throws Exception
	 */
	public static function markTestIncomplete(string $message = '') : void {}
	
	/**
	 * Mark the test as skipped.
	 *
	 * @param string $message
	 * @throws Exception
	 */
	public static function markTestSkipped(string $message = '') : void {}
	
	/**
	 * Return the current assertion count.
	 */
	public static function getCount() : int
	{
		return 0;
	}
	
	/**
	 * Reset the assertion counter.
	 */
	public static function resetCount() : void {}
	
	/**
	 * Returns a matcher that matches when the method is executed
	 * zero or more times.
	 */
	public static function any() : InvocationOrder
	{
		return new class() implements InvocationOrder {};
	}
	
	/**
	 * Returns a matcher that matches when the method is never executed.
	 */
	public static function never() : InvocationOrder
	{
		return new class() implements InvocationOrder {};
	}
	
	/**
	 * Returns a matcher that matches when the method is executed
	 * at least N times.
	 * @param integer $requiredInvocations
	 */
	public static function atLeast(int $requiredInvocations) : InvocationOrder
	{
		return new class() implements InvocationOrder {};
	}
	
	/**
	 * Returns a matcher that matches when the method is executed at least once.
	 */
	public static function atLeastOnce() : InvocationOrder
	{
		return new class() implements InvocationOrder {};
	}
	
	/**
	 * Returns a matcher that matches when the method is executed exactly once.
	 */
	public static function once() : InvocationOrder
	{
		return new class() implements InvocationOrder {};
	}
	
	/**
	 * Returns a matcher that matches when the method is executed
	 * exactly $count times.
	 * @param integer $count
	 */
	public static function exactly(int $count) : InvocationOrder
	{
		return new class() implements InvocationOrder {};
	}
	
	/**
	 * Returns a matcher that matches when the method is executed
	 * at most N times.
	 * @param integer $allowedInvocations
	 */
	public static function atMost(int $allowedInvocations) : InvocationOrder
	{
		return new class() implements InvocationOrder {};
	}
	
	/**
	 * @param mixed $value
	 * @return Stub
	 */
	public static function returnValue($value) : Stub
	{
		return new Stub();
	}
	
	/**
	 * @param array<integer|string, mixed> $valueMap
	 * @return Stub
	 */
	public static function returnValueMap(array $valueMap) : Stub
	{
		return new Stub();
	}
	
	public static function returnArgument(int $argumentIndex) : Stub
	{
		return new Stub();
	}
	
	/**
	 * @param callable $callback
	 * @return Stub
	 */
	public static function returnCallback($callback) : Stub
	{
		return new Stub();
	}
	
	/**
	 * Returns the current object.
	 *
	 * This method is useful when mocking a fluent interface.
	 */
	public static function returnSelf() : Stub
	{
		return new Stub();
	}
	
	public static function throwException(Throwable $exception) : Stub
	{
		return new Stub();
	}
	
	/**
	 * @param mixed ...$args
	 * @return Stub
	 */
	public static function onConsecutiveCalls(...$args) : Stub
	{
		return new Stub();
	}
	
	public function expectOutputRegex(string $expectedRegex) : void {}
	
	public function expectOutputString(string $expectedString) : void {}
	
	/**
	 * @phpstan-param class-string<\Throwable> $exception
	 * @param string $exception
	 */
	public function expectException(string $exception) : void {}
	
	/**
	 * @param integer|string $code
	 */
	public function expectExceptionCode($code) : void {}
	
	public function expectExceptionMessage(string $message) : void {}
	
	public function expectExceptionMessageMatches(string $regularExpression) : void {}
	
	/**
	 * Sets up an expectation for an exception to be raised by the code under test.
	 * Information for expected exception class, expected exception message, and
	 * expected exception code are retrieved from a given Exception object.
	 * @param Exception $exception
	 */
	public function expectExceptionObject(Exception $exception) : void {}
	
	public function expectNotToPerformAssertions() : void {}
	
	public function expectDeprecation() : void {}
	
	public function expectDeprecationMessage(string $message) : void {}
	
	public function expectDeprecationMessageMatches(string $regularExpression) : void {}
	
	public function expectNotice() : void {}
	
	public function expectNoticeMessage(string $message) : void {}
	
	public function expectNoticeMessageMatches(string $regularExpression) : void {}
	
	public function expectWarning() : void {}
	
	public function expectWarningMessage(string $message) : void {}
	
	public function expectWarningMessageMatches(string $regularExpression) : void {}
	
	public function expectError() : void {}
	
	public function expectErrorMessage(string $message) : void {}
	
	public function expectErrorMessageMatches(string $regularExpression) : void {}
	
	public function getStatus() : int
	{
		return 0;
	}
	
	public function markAsRisky() : void {}
	
	public function getStatusMessage() : string
	{
		return '';
	}
	
	public function hasFailed() : bool
	{
		return false;
	}
	
	/**
	 * Returns a builder object to create mock objects using a fluent interface.
	 *
	 * @param string|array<integer|string, string> $className
	 *
	 * @phpstan-template RealInstanceType of object
	 * @phpstan-param class-string<RealInstanceType>|array<integer|string, string> $className
	 * @phpstan-return MockBuilder<RealInstanceType>
	 * @psalm-suppress MixedReturnTypeCoercion
	 */
	public function getMockBuilder($className) : MockBuilder
	{
		return new MockBuilder();
	}
	
	/**
	 * Makes configurable stub for the specified class.
	 * 
	 * @phpstan-template RealInstanceType of object
	 * @phpstan-param class-string<RealInstanceType> $originalClassName
	 * @phpstan-return   Stub&RealInstanceType
	 * @psalm-suppress InvalidReturnType,InvalidReturnStatement
	 * @param string $originalClassName
	 */
	protected function createStub(string $originalClassName) : Stub
	{
		/** @phpstan-ignore-next-line */
		return new Stub();
	}
	
	/**
	 * Returns a mock object for the specified class.
	 *
	 * @param string|array<integer|string, string> $originalClassName
	 *
	 * @phpstan-template RealInstanceType of object
	 * @phpstan-param class-string<RealInstanceType>|array<integer|string, string> $originalClassName
	 * @phpstan-return MockObject&RealInstanceType
	 * @psalm-suppress InvalidReturnType,InvalidReturnStatement
	 */
	protected function createMock($originalClassName) : MockObject
	{
		/** @phpstan-ignore-next-line */
		return new MockObject();
	}
	
	/**
	 * Returns a configured mock object for the specified class.
	 *
	 * @param string|array<integer|string, string> $originalClassName
	 * @param array $configuration
	 *
	 * @phpstan-template RealInstanceType of object
	 * @phpstan-param class-string<RealInstanceType>|array<integer|string, string> $originalClassName
	 * @phpstan-return MockObject&RealInstanceType
	 * @psalm-suppress InvalidReturnType,InvalidReturnStatement
	 * @phpstan-ignore-next-line
	 */
	protected function createConfiguredMock($originalClassName, array $configuration) : MockObject
	{
		/** @phpstan-ignore-next-line */
		return new MockObject();
	}
	
	/**
	 * Returns a partial mock object for the specified class.
	 *
	 * @param string|array<integer|string, string> $originalClassName
	 * @param array<integer|string, string> $methods
	 *
	 * @phpstan-template RealInstanceType of object
	 * @phpstan-param class-string<RealInstanceType>|array<integer|string, string> $originalClassName
	 * @phpstan-return MockObject&RealInstanceType
	 * @psalm-suppress InvalidReturnType,InvalidReturnStatement
	 */
	protected function createPartialMock($originalClassName, array $methods) : MockObject
	{
		/** @phpstan-ignore-next-line */
		return new MockObject();
	}
	
	/**
	 * Returns a test proxy for the specified class.
	 *
	 * @phpstan-template RealInstanceType of object
	 * @phpstan-param class-string<RealInstanceType> $originalClassName
	 * @phpstan-return MockObject&RealInstanceType
	 * @psalm-suppress InvalidReturnType,InvalidReturnStatement
	 * @param string $originalClassName
	 * @param array $constructorArguments
	 * @phpstan-ignore-next-line
	 */
	protected function createTestProxy(string $originalClassName, array $constructorArguments = []) : MockObject
	{
		/** @phpstan-ignore-next-line */
		return new MockObject();
	}
	
	/**
	 * Mocks the specified class and returns the name of the mocked class.
	 *
	 * @param string $originalClassName
	 * @param array $methods
	 * @param array $arguments
	 * @param string $mockClassName
	 * @param boolean $callOriginalConstructor
	 * @param boolean $callOriginalClone
	 * @param boolean $callAutoload
	 * @param boolean $cloneArguments
	 *
	 * @phpstan-template RealInstanceType of object
	 * @phpstan-param class-string<RealInstanceType>|string $originalClassName
	 * @phpstan-return class-string<MockObject&RealInstanceType>
	 * @psalm-suppress MoreSpecificReturnType,LessSpecificReturnStatement
	 * @phpstan-ignore-next-line
	 */
	protected function getMockClass($originalClassName, $methods = [], array $arguments = [], $mockClassName = '', $callOriginalConstructor = false, $callOriginalClone = true, $callAutoload = true, $cloneArguments = false) : string
	{
		/** @phpstan-ignore-next-line */
		return $originalClassName;
	}
	
	/**
	 * Returns a mock object for the specified abstract class with all abstract
	 * methods of the class mocked. Concrete methods are not mocked by default.
	 * To mock concrete methods, use the 7th parameter ($mockedMethods).
	 *
	 * @param string $originalClassName
	 * @param array $arguments
	 * @param string $mockClassName
	 * @param boolean $callOriginalConstructor
	 * @param boolean $callOriginalClone
	 * @param boolean $callAutoload
	 * @param array $mockedMethods
	 * @param boolean $cloneArguments
	 *
	 * @phpstan-template RealInstanceType of object
	 * @phpstan-param class-string<RealInstanceType> $originalClassName
	 * @phpstan-return MockObject&RealInstanceType
	 * @psalm-suppress InvalidReturnType,InvalidReturnStatement
	 * @phpstan-ignore-next-line
	 */
	protected function getMockForAbstractClass($originalClassName, array $arguments = [], $mockClassName = '', $callOriginalConstructor = true, $callOriginalClone = true, $callAutoload = true, $mockedMethods = [], $cloneArguments = false) : MockObject
	{
		/** @phpstan-ignore-next-line */
		return new MockObject();
	}
	
	/**
	 * Returns a mock object based on the given WSDL file.
	 *
	 * @param string $wsdlFile
	 * @param string $originalClassName
	 * @param string $mockClassName
	 * @param array $methods
	 * @param boolean $callOriginalConstructor
	 * @param array $options An array of options passed to SOAPClient::_construct
	 *
	 * @phpstan-template RealInstanceType of object
	 * @phpstan-param class-string<RealInstanceType>|string $originalClassName
	 * @phpstan-return MockObject&RealInstanceType
	 * @psalm-suppress InvalidReturnType,InvalidReturnStatement
	 * @phpstan-ignore-next-line
	 */
	protected function getMockFromWsdl($wsdlFile, $originalClassName = '', $mockClassName = '', array $methods = [], $callOriginalConstructor = true, array $options = []) : MockObject
	{
		/** @phpstan-ignore-next-line */
		return new MockObject();
	}
	
	/**
	 * Returns a mock object for the specified trait with all abstract methods
	 * of the trait mocked. Concrete methods to mock can be specified with the
	 * `$mockedMethods` parameter.
	 *
	 * @param string $traitName
	 * @param array $arguments
	 * @param string $mockClassName
	 * @param boolean $callOriginalConstructor
	 * @param boolean $callOriginalClone
	 * @param boolean $callAutoload
	 * @param array $mockedMethods
	 * @param boolean $cloneArguments
	 * @phpstan-ignore-next-line
	 */
	protected function getMockForTrait($traitName, array $arguments = [], $mockClassName = '', $callOriginalConstructor = true, $callOriginalClone = true, $callAutoload = true, $mockedMethods = [], $cloneArguments = false) : MockObject
	{
		return new MockObject();
	}
	
	/**
	 * Returns an object for the specified trait.
	 *
	 * @param string $traitName
	 * @param array<integer|string, mixed> $arguments
	 * @param string $traitClassName
	 * @param boolean $callOriginalConstructor
	 * @param boolean $callOriginalClone
	 * @param boolean $callAutoload
	 *
	 * @return object
	 */
	protected function getObjectForTrait($traitName, array $arguments = [], $traitClassName = '', $callOriginalConstructor = true, $callOriginalClone = true, $callAutoload = true)// : object
	{
		return new stdClass();
	}
	
	/**
	 * This method is called before the first test of this test class is run.
	 */
	public static function setUpBeforeClass() : void {}
	
	/**
	 * This method is called after the last test of this test class is run.
	 */
	public static function tearDownAfterClass() : void {}
	
	/**
	 * This method is called before each test.
	 */
	protected function setUp() : void {}
	
	/**
	 * Performs assertions shared by all tests of a test case.
	 *
	 * This method is called between setUp() and test.
	 */
	protected function assertPreConditions() : void {}
	
	/**
	 * Performs assertions shared by all tests of a test case.
	 *
	 * This method is called between test and tearDown().
	 */
	protected function assertPostConditions() : void {}
	
	/**
	 * This method is called after each test.
	 */
	protected function tearDown() : void {}
	
}
